public interface ActiveInterface {
    void isActive();

    default String yourName() {
        return "No name";
    }
}
