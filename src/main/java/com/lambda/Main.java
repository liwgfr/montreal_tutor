package com.lambda;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Лямбда выражения в Java это своеобразная имитация функция (функционального программирования)
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();

        Calc calc = (x, y) -> x + y; // пускай реализация нашего интерфейса Calc будет выступать в виде суммы двух параметров (x + y)
        System.out.println(calc.res(a, b)); // 2 1 = 3
        // Лямбда выражения применимы только в функциональным интерфейсам.
    }
}

interface Calc { // функциональный интерфейс
    int res(int x, int y); // метод, который не имеет на данном этапе реализации, возвращает число (int), принимает два целых числа.
}
