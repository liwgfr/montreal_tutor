package com;

import java.util.Scanner;

public class Recursion {
    // Подсчитать факториал (до 20)
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int fact = 1; // 1
        int number = in.nextInt(); //
        for (int i = 1; i <= number; i++) { // итеративное решение
            fact = fact * i; // 5! -> 1*2*3*4*5 -> П_1..n a[i]
        }

        int factByRecursion = factorial(number);
        System.out.println(fact);
        System.out.println(factByRecursion);
    }

    static int factorial(int number) { // 5
        if (number == 0) { // Условие, на котором наша рекурсия заканчивается
            System.out.println("Returned 1!");
            return 1; // стоп-условие для нашей рекурсии
        } else {
            System.out.println("n " + number);
            return (number * factorial(number - 1));
            // 1 * 2 * 3 * 4 * 5
            // 5 * fact(5-1)
            // 4 * fact(4-1)
            // 3 * fact(3-1)
            // 2 * fact(2-1)
            // 1 * fact(1-1)
            // return 1;
        }
    }
}
