package com.acm;

import java.io.*;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int sum = 0;
        int n = in.nextInt(); //nextInt() -> целое число
        for (int i = 0; i < n; i++) {
            sum += in.nextInt();
        }
        System.out.println(sum);
        
        /*
        int sum = 0; // ok
        Scanner input = new Scanner(new File("/home/binocla/IdeaProjects/montreal_tutor/src/com/acm/input.txt"));
        String[] numbers = new String[0];
        while (input.hasNext()) {
            String line = input.nextLine();
            numbers = line.split(" ");
            System.out.println(numbers.length);
            if (numbers.length < 2) {
                continue;
            }
            for (int i = 0; i < numbers.length; i++) {
                sum += Integer.parseInt(numbers[i]);
            }
        }
        System.out.println(sum);
        PrintWriter pr = new PrintWriter("output.txt");
        pr.println(sum);
        pr.close(); // todo - рассказать (IO / NIO)
         */
    }
}

