package com.acm;

import java.util.Scanner;

public class Task7 {

    public static void toBinary(int decimal) {
        int[] binary = new int[40];
        int index = 0;
        int quantityOfZeros = 0;
        while (decimal > 0) {
            binary[index++] = decimal % 2;
            decimal = decimal / 2;
        }
        for (int i = index - 1; i >= 0; i--) {
            System.out.print(binary[i]); // 1 0 1 0
        }
        for (int i = index - 1; i >= 0; i--) {
            System.out.println("attempt " + binary[i]); // В качестве простого варианта проверки имеет смысл добавлять промежуточные выводы и смотреть на состояние переменной (переменных)
            // 0101
            if (binary[i] == 0) {
                quantityOfZeros++;
            } else break;
        }
        System.out.println();
        System.out.println(quantityOfZeros);

    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter integer: ");
        int n = input.nextInt();
        String s = Integer.toBinaryString(n);
        int cnt = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == '0') {
                cnt++;
            } else {
                break;
            }
        }
        System.out.println(cnt);

        System.out.println();
        System.out.println("Binary equivalent is: ");
        toBinary(n);
    }
}
