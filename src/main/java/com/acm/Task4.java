package com.acm;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }
        /*
        Scanner input = new Scanner(new File("input.txt"));
        while (input.hasNext()) {
            int year = input.nextInt();
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                System.out.println(1);
            } else {
                System.out.println(0);
            }
        }

         */
    }
}
