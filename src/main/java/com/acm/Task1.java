package com.acm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) throws FileNotFoundException {
        // Scanner in = new Scanner(System.in); // System.in -> ввод с консоли
        // System.out.println(in.nextInt() + in.nextInt());

        Scanner input = new Scanner(new File("/home/binocla/IdeaProjects/montreal_tutor/src/com/acm/input.txt"));
        // String line = input.nextLine();
        // String[] numbers = line.split(" ");
        // int output = Integer.parseInt(numbers[0]) + Integer.parseInt(numbers[1]);
        int output = input.nextInt() + input.nextInt() + input.nextInt(); // input.nextInt() -> он считывает только числа (минуя все пробелы)

        System.out.println(output);
        PrintWriter pw = new PrintWriter("output.txt");
        pw.println(output);
        pw.close(); // закрывать PrintWriter обязательно (потому что если не закроем - он не сохранит вывод)

    }
}

