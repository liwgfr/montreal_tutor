package com.acm;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your age: ");
        int n = input.nextInt();
        int m;
        if (n < 7) {
            System.out.println("preschool child");
        }
        if (n >= 7 && n <= 17) {
            m = n - 6;
            System.out.println("schoolchild " + m);
        }
        if (n > 17 && n <= 22) {
            m = n - 17;
            System.out.println("student " + m);
        } else if (n > 22) {
            System.out.println("specialist");
        }
    }
}
