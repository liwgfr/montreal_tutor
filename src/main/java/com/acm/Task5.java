package com.acm;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter quantity of elements: ");
        int n = input.nextInt();
        System.out.println("Please enter " + n + " integers: ");
        int min = input.nextInt();
        int index = 1;
        for (int i = 2; i < n + 1; i++) {
            int check = input.nextInt();
            if (min > check) {
                min = check;
                index = i;
            }
        }
        System.out.println("Human Index of smallest number is:" + index);
    }
}
