package com.comparators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;

public class Comparators {
    // Comparable -> свойство сравниваться. Functional Interface. Есть один метод compareTo(T o) (возвращает int). >0 -> a > b (a.compareTo(b)), если <0 b > a, если =0 (a == b)
    // Comparator -> Это реализация сравнения. Есть метод compare(T o1, T o2) -> умеет сравнивать по нескольким аттрибутам (полям)
    public static void main(String[] args) {
        // Integer, Double, Character, Float, Long... -> классы обертки для примитивов
        // 1) Расширение функционала примитивов
        // 2) Возможность использования в коллекциях
        // Project Loom, которая позволяет примитивы пихать <int> (?)
        Integer x1 = 50;
        Integer x2 = 50;
        System.out.println(x1 == x2);


        Comparable<Integer> comp;
        Integer a = 400; // ссылочный тип данных (== по ссылке за исключением значений, когда число равно -128...127 -> Integer pool)
        Integer b = 400;
        System.out.println(a.compareTo(b)); // 0 (сравнивает ЗНАЧЕНИЯ)
        a = 1000;
        b = -2;
        System.out.println(a.compareTo(b)); // 1, a > b
        a = 11111;
        b = 5000000;
        System.out.println(a.compareTo(b)); // -1, a < b

        ArrayList<Person> list = new ArrayList<>(Arrays.asList(new Person("Sergey", 19),
                new Person("Sergey", 21),
                new Person("Sergey", 15),
                new Person("Binocla", 25),
                new Person("Qweqwe", 15),
                new Person("qweqwe", 23)));
        System.out.println(list);

        list.sort(new Person());
        System.out.println(list);

        TreeSet<Person> set = new TreeSet<>(new Person());
        set.addAll(Arrays.asList(new Person("Sergey", 19),
                        new Person("Sergey", 21),
                        new Person("Sergey", 15),
                        new Person("Binocla", 25),
                        new Person("Qweqwe", 15),
                        new Person("qweqwe", 23)));
        System.out.println(set);



    }
}
