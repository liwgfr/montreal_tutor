package com.comparators;

import java.util.Comparator;
import java.util.StringJoiner;

public class Person implements Comparator<Person> {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compare(Person o1, Person o2) {
        if (o1.name.length() > o2.name.length() || o1.age > o2.age) {
            return 1;
        } else if (o1.name.length() < o2.name.length() || o1.age < o2.age) {
            return -1;
        } else {
            return 0; // key1 == key2
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age=" + age)
                .toString();
    }
}
