package com;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class SetExample {
    public static void main(String[] args) {
        // HashMap vs LinkedHashMap vs TreeMap
        // .add
        // .put
        // Map vs HashMap
        // Map (I) -> HashMap (C) implements Map
        // Map != Collection
        // Collection (I) -> Set (I), List (I), Queue (I) -> Set -> HashSet, LinkedHashSet... | List -> ArrayList, LinkedList... | Queue -> Stack, ArrayDeque...
        LinkedHashSet<String> set = new LinkedHashSet<>();
        set.add("qwe");
        set.add("asd");
        set.add("abc");

        // "asd" -> position
        // set.toArray() -> превратили в массив
        for (int i = 0; i < set.size(); i++) {
            if (set.toArray()[i].equals("asd")) {
                System.out.println(i);
            }
        }

        // 2) forEach
        int cnt = 0;
        for (String s : set) {
            if (s.equals("asd")) {
                System.out.println(cnt);
            }
            cnt++;
        }
    }
}
