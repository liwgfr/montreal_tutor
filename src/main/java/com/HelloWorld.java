package com;

import java.util.Arrays;
import java.util.Scanner;

public class HelloWorld {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter phrase: ");
        String phrase = input.nextLine();
        System.out.println("Quantity of distinct symbols in that phrase are: ");
        phrase.lines().forEach(x -> System.out.print(x + " "));
        System.out.println();
        phrase = phrase.replaceAll("\\s+", "");
        // [а-яА-Я]
        System.out.println(phrase.chars().distinct().count()); // chars() -> преобразует каждый символ к unicode int.
        // chars() потому что нас интересовали символы, а не слова

        Scanner in = new Scanner(System.in);
        System.out.println("Please enter second phrase: ");
        String line = in.nextLine();
        System.out.println("Quantity of distinct words in that phrase are: ");
        String[] array = line.split(" ");
        System.out.println(Arrays.stream(array).map (x -> x.toLowerCase()).distinct().count());

    }
}