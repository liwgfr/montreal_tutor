package com.generic;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Database<? extends Number> db = new Database<>(); // передали 2 в конструктор
//        db.addT(-2); // doubleValue() -> -2 ? -2.0
//        db.addT(-10); // doubleValue() -> -2 ? -2.0
//        db.addT(3.5);
        db.getAllT();
        Cat c = new Cat();
        Dog d = new Dog();
        Animal a = new Animal();
        List<? super Cat> list = new ArrayList<>(); //
        list.add(c);
//        list.add(a);
//        list.add(d);
        System.out.println(list);

    }
}
