package com.generic;

import java.util.ArrayList;

// Generic (Обобщения)
public class Database<T> { // Что при создании экземпляра класса Database мы указываем тип нашего класса (Integer, Person, Double, Scanner...)
    T obj; // new T() -> запрещено (потому что такого типа на данный момент еще не существует)
    ArrayList<T> list = new ArrayList<>();
    // extends -> T extends Number // ковариантность. T extends Number -> что в качестве типа данных может быть все, что унаследовано от Number
    // super -> T super Number контр вариантность T super Number -> в качестве типа данных может быть либо Number, либо все, что выше Object
    // инвариантность

    // Animal
    // Dog extends Animal
    // Cat extends Animal
    // HomeCat extends Cat
    // HomeDog extends Dog
    // Wolf extends Dog
    // Penguin
    // ArrayList<T extends Cat> -> Cat, HomeCat ok
    // ArrayList<T extends Animal> -> Dog, Cat, HomeCat, HomeDog, Wolf ok
    // ArrayList<T extends Penguin> -> Penguin ok
    // ArrayList<T super Penguin> -> Penguin, Object ok
    // ArrayList<T super Dog> -> Animal, Dog, Object ok
    // ArrayList<T> -> все ok

    // <?> -> Wildcard (специальная фишка).
    // Если класс Q наследник класса M, то Collection<Q> НЕ наследник Collection<M>

    public T getObj() { // getter
        return obj;
    }

    public void addT(T obj) {
        list.add(obj);
    }

    public void getAllT() {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
    // Это сработает только при T extends Number
//    public double sumOfNumbers() {
//        double sum = 0;
//        for (Number n : list) {
//            sum += n.doubleValue();
//        }
//        return sum;
//    }

}
