package com;

import java.util.*;

public class Sets {
    // Set - интерфейс extends Collection
    public static void main(String[] args) {
        // Set -> множество. Множество - это набор однотипных данных, но без повторений. {1, 5, 4, 2, 2, 1} -> {1, 5, 4, 2}

        Set<Integer> set = new HashSet<>(); // Порядок не сохраняет
        Set<Integer> set1 = new LinkedHashSet<>(); // Как ввели - так и вывели
        Set<Integer> set2 = new TreeSet<>(); // Основан на деревьях. По умолчанию сортирует по возрастанию

        // {1, 2, 3, 3, 3} -> {1, 2, 3}. {1:null, 2:null, 3:null} Map<Key, Value> "null"
        // add - добавить значение в Set
        // get у Set нет!!!
        // isEmpty()
        // remove
        set1.add(5);
        set1.add(2);
        System.out.println(set1.toArray()[1]);

        set2.add(5);
        set2.add(4);
        set2.add(3);
        set2.add(3);
        set2.add(3);
        set2.add(3);
        set2.add(2);
        set2.add(2);
        set2.add(1);
        set2.add(1);
        set2.add(1);
        set2.add(1);
        set2.add(1);
        Object o = new Object();
        System.out.println(set2);
        System.out.println();

        Scanner in = new Scanner(System.in);
        HashSet<String> a = new HashSet<>();
        in.nextLine();
        String[] s = in.nextLine().toLowerCase().split(" ");
        for (String y : s) {
            a.add(y);
        }
        System.out.println(s.length - a.size());

    }
    /**
     Вводится число N - количество будущих строк
     Далее вводятся N строк (регистр может быть разным)
     Необходимо вывести количество повторившихся строк

     Aba aBa Aba caba q
     Вывод:
     2
     */
}
