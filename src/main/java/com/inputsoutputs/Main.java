package com.inputsoutputs;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        // 1)
        Scanner in = new Scanner(new FileInputStream("src/com/inputsoutputs/file.txt")); // System.in
        while (in.hasNext()) {
            System.out.println(in.next());
        }
        // 2)
        InputStream s; // один из самых главных абстрактных классов отвечающих за считывание инфы
        // *Приставка*InputStream -> считывание происходит в виде байтов
        // Считывание файла, картинки, аудио, ... binary data (byte)
        // InputStream - сам по себе абстрактный класс InputStream stream = new InputStream();
        // FileInputStream - позволяет работать (считывать) из файла
        // ImageInputStream - позволяет обрабатывать картинку
        // DataInputStream - позволяет как раз преобразовывать байты в тип данных


        // Такая вложенность позволяет наращивать функционал (1 Позволяет просто работать с файлом -> 2 Байты конвертировать в тип данных)
        DataInputStream inp = new DataInputStream(new FileInputStream("src/com/inputsoutputs/file.txt"));
        FileInputStream x = new FileInputStream("src/com/inputsoutputs/file.txt");
        while (x.read() != -1) {
            System.out.println((char) x.read());
        }

        // while (inp.read() != -1) { // inp.read() -1 означает что мы дошли до конца файла
            System.out.println(inp.readLine()); // readUTF -> позволяет считывать строку
        // }

        // 3)
        Reader r; // Не байтоориентирован, а символоориентован.
        // *Приставка Reader* говорит что данный класс символоориентирован
        BufferedReader f = new BufferedReader(new FileReader("src/com/inputsoutputs/file.txt"));
        FileReader fr = new FileReader("src/com/inputsoutputs/file.txt");
        char[] chars = new char[4];
        while (fr.read() != -1) {
            System.out.println(fr.read(chars));
        }
        for (int i = 0; i < chars.length; i++) {
            System.out.println(chars[i]);
        }
        while (f.read() != -1) {
            System.out.println(f.readLine());
        }

        String str = "Hello";

        // *Buffered* приставка позволяет сказать, что считываемая нами информация хранится в буфере.
        // Буфер - кэш (временное хранилище) для наших байтов/текста (информации)
        // Буфер хранит в себе информацию файла.

        // 1)
        FileOutputStream output = new FileOutputStream("src/com/inputsoutputs/output.txt");
        output.write(str.getBytes());
        output.flush(); // flush() -> вытолкни из буфера всю информацию
        output.close(); // закрыть доступ к выводу

        DataOutputStream output1 = new DataOutputStream(new FileOutputStream("src/com/inputsoutputs/output.txt"));
        output1.writeUTF(str);
        output1.flush();
        output1.close();

        // 2) Reader - считывать, Writer -> писать символоориентированно
        PrintWriter pw = new PrintWriter("src/com/inputsoutputs/qwe.txt");
        pw.println(str);
        pw.flush();
        pw.close();

        // BufferedReader + BufferedWriter
        // BufferedReader + PrintWriter
        // Scanner + PrintWriter
    }
}
