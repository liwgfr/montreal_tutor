package com.iterators;

import java.util.Arrays;
import java.util.Iterator;

public class Person<T> implements Iterable<T> {
    T[] ar; // Container, Node -> array

    public Person(T[] ar) { // принимает массив и заполняет в поле
        this.ar = ar;
    }

    @Override
    public Iterator<T> iterator() { // возвращаем итератор
        return new Itr();
    }

    @Override
    public String toString() {
        return "Person{" +
                "ar=" + Arrays.toString(ar) +
                '}';
    }

    private class Itr implements Iterator<T> {
        int idx = 0; // стартовая позиция в массиве

        @Override
        public boolean hasNext() { // false, если не дошли до конца, иначе - true
            return idx != ar.length; // true, cursor == null -> false
        }

        @Override
        public T next() { // возвращаем текущий элемент
            return ar[idx++]; // ar[0] -> ar[1] ... hasNext = true
        }
    }

}
