package com.iterators;

import java.util.ArrayList;
import java.util.Iterator;

public class Iterators {
    // Iterator -> интерфейс, у которого три основных метода: T (String, Integer, Object) next(), boolean hasNext(), void remove() -> удаляет текущий
    // Iterable -> функциональный интерфейс (в котором есть только один нереализованный метод). Iterator<T> iterator() -> возвращает Iterator
    // Iterable -> это свойство нашего класса быть итерируемым (грубо говоря, мы указываем, что по нашему классу может бегать цикл forEach)
    // Iterator -> это реализация нашего свойства быть итерируемым.

    public static void main(String[] args) {
        ArrayList<Integer> ar = new ArrayList<>();
        ar.add(2);
        ar.add(1);
        ar.add(5);
        ar.add(10);

        // 1) forEach (более новая, внутри под капотом на самом деле используется Iterator)
        for (Integer x : ar) {
            System.out.println(x);
        }
        System.out.println();
        // 2) (более старый, с явным применением Iterator)
        Iterator<Integer> it = ar.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        System.out.println();

        Person<Integer> p = new Person<>(new Integer[] {1, -10, 5, 200});
        for (Integer integer : p) {
            System.out.println(integer);
        }

    }
}
