package streamapi;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    // Stream API
    // I/O Stream -> надо закрывать, после того как закрыли нужно создать новый экземпляр
    // Stream API [x] // api - упрощенная схема доступа к какой-либо библиотеке/серверу/фреймворку
    // -----
    // Library -> совокупность классов
    // Framework -> совокупность библиотек
    // server == host -> telegram api / vk api. API -> интерфейс для взаимодействия с платформой Телеграм.
    // api/v1/sendMessage?id=1&message=Hello -> сообщение отправится человеку под id = 1
    // -----
    // Concurrency API (threads / stream)


    // Stream API - потоки в Java, которые направлены на упрощение кода и делают его более емким и удобным в плане читабельности
    // Java 8+ (?)
    // Три стадии:
    // 1) Инициализация
    // 2) Конвейерные методы (промежуточные методы) .method1().method2().method1()....
    // 3) Терминальная (метод, который закрывает поток и после терминального метода невозможно продолжить использование потока) Аналогия: close()
    // Stream<T> -> Generic интерфейс (Stream<String>) -> универсальный
    // IntStream -> стрим из Integer (некоторые "фишки") -> расширенный, но только Integer
    // LongStream -> стрим из Long (некоторые "фишки") -> расширенный, но только Long
    // DoubleStream -> стрим из Double (некоторые "фишки") -> расширенный, но только Double
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Даны числа {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}.
        // 1) Посчитать количество четных
        // 2) Вывести все элементы кратные 3
        // 3) Вывести все элементы, предварительно умножив их на 3, кратные 4

        // 1
        int[] ar = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int cnt = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] % 2 == 0) {
                cnt++;
            }
        }
        System.out.println(cnt);

        // 2
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] % 3 == 0) {
                System.out.print(ar[i] + " ");
            }
        }
        System.out.println();
        // 3
        for (int i = 0; i < ar.length; i++) {
            ar[i] = ar[i] * 3;
            if (ar[i] % 4 == 0) {
                System.out.print(ar[i] + " ");
            }
        }
        int[] ar2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println();
        // 1
        System.out.println(Arrays.stream(ar2).filter(x -> x % 2 == 0).count());
        // 2
        Arrays.stream(ar2).filter(x -> x % 3 == 0).forEach(x -> System.out.print(x + " "));
        System.out.println();
        // 3
        Arrays.stream(ar2).map(x -> x * 3).filter(x -> x % 4 == 0).forEach(x -> System.out.print(x + " "));

        // Инициализация стрима
        // 1
        // int x = in.nextInt();
        // Stream.of(x, in.nextLine(), in.nextDouble(), 5, "qwe", 5.4); // -> стрим из Object
        // 2
        Stream<String> stream = Stream.of("1", "2", "3", "2"); // -> стрим, но из конкретного типа данных
        stream = stream.filter(x -> x.equals("12"));
        //...
        stream.count();
        // 3
        // .stream() -> поддерживается у коллекций, массивов (через класс Arrays), у строк .lines() ...
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3));
        a.stream();

        String s = "Hello World";
        s.lines(); // .stream()

        // 2 Конвейерные (промежуточные) операции / методы

        // Stream.of(1, 2, 3).map(x -> x * 3 + 5 / 12) : {1, 2, 3} -> {3, 6, 9}. map -> отображение (mapping) - выполняет какую-то операцию (математическую, .map(x -> Integer.parseInt(x))) по отношению к каждому элементу
        // map -> преобразование к чему-либо
        // A -> B
        // Stream.of(1, 2, 3, 4).filter(x -> x % 2 == 0).map(x -> x * 2) : Аналог - if. -> каждый четный элемент умножает на 2
        // Stream.of(1, 2, 3, 4).map(x -> x * 2).filter(x -> x % 2 == 0) : все умножили на два, а потом только решили проверить (отфильтровать)
        // filter -> если true -> идем дальше, если нет - отсеиваем (пропускаем)
        // .distinct() -> убирает все повторяющиеся элементы ({1, 2, 1 ,1} -> {1, 2})
        // .peek(x -> System.out.println(x)) -> forEach
        /*
        for (int i = 0; i < n; i++) {
            System.out.println(ar[i]);
        }

        Arrays.stream(ar).peek(x -> System.out.println(x + " "));
         */
        // .limit(5) -> ограничивает выборку. {2, 2, 2, 2, 2, 2, 2} -> .limit(3).map(x -> x * 15).forEach(x -> System.out.println(x)) : умножает только первые три и выводит три
        // .limit(5) -> ограничивает выборку. {2, 2, 2, 2, 2, 2, 2} -> .map(x -> x * 15).limit(3).forEach(x -> System.out.println(x)) : умножит все, выведет три
//        System.out.println();
//        Stream.of(1, 2, 3, 4, 5, 2, 7, 2, 2, 10, 5, 120)
//                .filter(item -> item % 2 == 0) // {2, 4, 2, 2, 2, 10, 120}
//                .limit(3)  // 3 -> {2, 4, 2}
//                .peek(item -> System.out.print("Peek " + item + " "))
//                .map(item -> item * 10) // {2 * 10, 4 * 10, 2 * 10}
//                // .limit(2) // {20, 40}
//                .forEach(item -> System.out.print(item + " "));
        // skip(5) -> пропускает 5 элементов
        // .sorted(Comparator ...)
        // Comparator.naturalOrder(); // по возрастанию
        // Comparator.reverseOrder(); // по убыванию
        System.out.println();
        Stream.of(1515, 32, 155, 3, -2, 13, 10).sorted(Comparator.naturalOrder()).forEach(x -> System.out.println(x));

        // 3
        // collect(Collectors) -> собирает в какую-то коллекцию (или вообще во что-то, например, в строку, в мапу, в arraylist...)
        // Collectors - это класс, предоставляющий возможность "собирания"
        List<Integer> list = Stream.of(1, 2, 3, 4, 5, 10, 31)
                .filter(x -> x % 2 != 0)
                .map(x -> x + 5)
                .collect(Collectors.toList());

        ArrayList<Integer> list1 = Stream.of(1, 2, 3, 4, 5, 10, 31)
                .filter(x -> x % 2 != 0)
                .map(x -> x + 5)
                .collect(Collectors.toCollection(() -> new ArrayList<>())); // () -> просто сделай (не надо брать какую-то переменную/объект, просто возьми и сделай это действие
        System.out.println(list1);

        // count() -> возвращает количество (long)
        long count = Stream.of(1, 2, 3, 4, 5, 6)
                .filter(x -> x % 2 == 0)
                .filter(x -> x < 5)
                .count();
        System.out.println(count);

        // sum() - IntStream/DoubleStream/LongStream
        // max()
        // min()
        System.out.println(DoubleStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).sum());
        // Stream<Integer> str = Stream.of(null, null).max();
        System.out.println(Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> Integer.parseInt(String.valueOf(x))).max(Comparator.naturalOrder()).get());
        System.out.println(Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> Integer.parseInt(String.valueOf(x))).min(Comparator.naturalOrder()).get());
        // forEach() == peek, только терминальный
        Stream.of("qwe", "aba", "caba").forEach(x -> System.out.println(x));
        // flatMap
        // reduce ...

        // flatMap -> конвейерный метод (похож на .map) x -> x * 5
        // .map : 1 -> 1. 5 -> 50. "1" -> 1. One-to-One
        // flatMap : 1 -> 1,2,3 || 1 -> 0 || 1 -> 1. 15 -> 1, 5, 10, 15 || 15 -> null ("") || 15 -> 150
        // flatMap: One-to-Many, One-to-One, One-to-Null
        // flatMap(x -> Тут обязательно Stream)
        System.out.println("FlatMap!");
        Stream.of(1, 2, 3, 4, 5) // {1, 2, 3...}
                // 1 -> 0 1
                // 1 ->
                // [0, x)
                // 1 -> 0
                // 2 -> 0 1
                // 3 -> 0 1 2
                // ...
                .flatMapToInt(x -> IntStream.range(0, x)) // flatMap Stream<Integer> -> IntStream
                .forEach(x -> System.out.print(x + " "));
        // reduce -> reduce по какому-то параметру "соединяет" наш стрим:
        // reduce - терминальный метод
        // {1, 2, 3, 4, 5} .reduce(+) -> 15
        Stream<Integer> reduceCheck = Stream.of(1, 2, 3, 4, 5, 6);
        System.out.println("Reduce");
        System.out.println(reduceCheck.reduce((x, y) -> x - 3 + 5 * 10 - 11 + y).get());

        // Optional<T> -> класс, Java8+. Optional позволяет избегать NullPointerException'ы
        // Каким образом? Optional (считай его как фантик) позволяет обработать null при его наличии
        // Если null -> Выведи "Нет данных"
        String string = "binocla";
        String string1 = null;
        Optional<String> opt = Optional.of(string); // non-null де-факто
        Optional<String> opt1 = Optional.ofNullable(string1); // null де-факто
        System.out.println(opt); // Optional[string]
        System.out.println(opt.get()); // метод get() позволяет достать из Optional значение -> string
        System.out.println(opt.isEmpty()); // isEmpty() -> а есть ли что-то в Optional? true - если пусто
        System.out.println(opt.isPresent()); // isPresent() -> а есть ли что-то в Optional? true - если не пусто
        System.out.println(opt1.orElse(string)); // orElse -> если пусто - верни что-то другое ("Нет данных")
        // Работа с базами данных (Sergey -> response Optional.ofNullable(response).orElse("Такого пользователя нет"));

        /* todo
            Дана строка (nextLine) -> 1)
            вывести количество уникальных символов
            2) Количество уникальных слов (регистр должен быть не важен, aniMon == AnImOn)
         */

    }
}
