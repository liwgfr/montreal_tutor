import java.util.Arrays;
import java.util.Scanner;

public class BinoclarSolution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Прошу ввести количество букв для проверки:");
        int count = in.nextInt();
        System.out.println("Прошу ввести " + count + " букв для сортировки: ");
        in.nextLine();
        String input = in.nextLine();
        Character[] letters = new Character[count];
        if (input.length() < count) {
            System.out.println("Вы ввели недостаточное количество букв, вероятно вы сами пьяны ;) Ничего я вам сортировать не буду.");
            return;
        }
        for (int i = 0; i < count; i++) {
            letters[i] = input.charAt(i);
        }
        //test
        System.out.println(Arrays.toString(letters));
        // ё -> 1251
        // а-я 1200-1232
        // отдельно рассмотреть случай с этой буквой
        // flag | double е -> 2, ж -> 3; ё -> 2.5 | ...
        for (int i = 0; i < count; i++) {
            for (int j = i; j < count; j++) {
                if (letters[i] > letters[j]) {
                    char temp = letters[i];
                    letters[i] = letters[j];
                    letters[j] = temp;
                }
            }
        }
        System.out.println("Вывод: ");
        for (char y : letters) {
            System.out.print(y + " ");
        }
        System.out.println();
        for (int i = letters.length - 1; i >= 0; i--) {
            System.out.print(letters[i] + " ");
        }
    }
}