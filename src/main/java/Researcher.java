public class Researcher extends Female {
    private String fieldOfWork;
    private boolean isDone;

    public Researcher(String name, int age, char gender, String email, boolean hasCat, boolean hasPhone,
                      String fieldOfWork, boolean isDone) {
        super(name, age, gender, email, hasCat, hasPhone);
        this.fieldOfWork = fieldOfWork;
        this.isDone = isDone;
    }
}