import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Maps {
    public static void main(String[] args) {
        // BubbleSort -> O(n^2)
//        int n = 10;
//        int[] ar = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        for (int i = 1; i < n; i++) {
//            for (int j = 0; j < n; j++) {
//                if (ar[i] < ar[j]) {
//                    int temp = ar[i];
//                    ar[i] = ar[j];
//                    ar[j] = temp;
//                }
//            }
//        }

        // Map -> Интерфейс (подобно List, Collection)
        // Map<K, V> -> хранит данные в качестве связки ключ-значение
        // Если ключи повторяются (то есть совпадают), то предыдущий удаляется и заменяется таким же новым (но с его значением)
        // в HashMap в качестве ключа может быть null. В качестве значения - тоже может быть null.
        // В HashTable ключом null быть не может!!
        // HashMap потоко небезопасный, HashTable потокобезопасный
        // put(K, V) -> положить (аналог add)
        // get(K) -> получить значение по ключу
        // remove(K) -> удалить ключ и его значение
        // containsKey(K) -> содержит ли ключ?
        // containsValue(V) -> содержит ли значение?
        // keySet() -> возвращает Set ключей (массив ключей)
        // values() -> возвращает Collection (массив значений)
        // entrySet() -> Возвращает Set из ключей и значений
        // size() -> кол-во ключей
        ArrayList<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        list.add(10);
        HashMap<String, ArrayList<Integer>> map = new HashMap<>();
        map.put(null, list);
        ArrayList<Integer> list1 = (ArrayList<Integer>) list.clone();
        list1.clear();
        list1.add(-2);
        list1.add(123);
        list1.add(555);
        map.put("q", list1);
        ArrayList<Integer> list2 = (ArrayList<Integer>) list.clone();
        list2.clear();
        list2.add(5);
        list2.add(10);
        list2.add(123123);
        map.put("w", list2);

        System.out.println(map);

        // forEach -> модернизированный цикл fori, Который очень удобен при работе с коллекциями
        for (ArrayList<Integer> s: map.values()) { // Бежим по значениям Map
            System.out.println(s);
        }
        for (String s : map.keySet()) {
            System.out.println(s + " " + map.get(s));
        }
        for (Map.Entry<String, ArrayList<Integer>> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
            // Map.Entry -> интерфейс, Который позволяет пробегаться по entrySet()
        }
        Map<String, Integer> mapInterface = new HashMap<>();

        // HashMap -> реализация интерфейса Map // порядок НЕ СОХРАНЯЕТ!!!
        // TreeMap -> реализация интерфейса Map // автоматически сортирует ключи по возрастанию
        // LinkedHashMap -> реализация интерфейса Map // порядок ввода СОХРАНЯЕТ

        HashMap<String, Integer> map1 = new HashMap<>(); // порядок не сохраняет!
        LinkedHashMap<String, Integer> map2 = new LinkedHashMap<>(); // сохраняет порядок ввода ключей на выводе
        // Hash -> подразумевает использование специальной хэш-функции (hashCode)
        map1.put("hello", 2);
        map1.put("hi", 3);
        map1.put("hello", 10);
        map1.put("qwe", -2);
        map1.put("hi", -3);
        map1.put("hello", 300);
        System.out.println(map1);
        map2.put("hello", 2);
        map2.put("hi", 3);
        map2.put("hello", 10);
        map2.put("qwe", -2);
        map2.put("hi", -3);
        map2.put("hello", 300);
        System.out.println(map2);

    }
}
