package concurrency;

public class SimpleRunnable implements Runnable {
    long result = 2;
    public static void main(String[] args) {
        Thread t1 = new Thread(new SimpleRunnable()); // Передали реализацию интерфейса Runnable -> run()
        t1.start();
    }

    @Override
    public void run() { // этот метод описывает поведение каждого потока (psvm)
        for (int i = 0; i < 7; i++) {
            result *= Math.pow(result, result);
            System.out.println(Thread.currentThread().getName() + " " + result); // Вывожу название потока и результат
            // Thread.sleep(1000); // Усыпляю поток (перевожу в режим ожидания) на секунду
        }
    }
}
