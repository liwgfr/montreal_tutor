package concurrency;

import lombok.SneakyThrows;

public class SimpleThreadsWithoutInheritance {
    private int result = 10;
    @SneakyThrows
    public static void main(String[] args) {
        SimpleThreadsWithoutInheritance a = new SimpleThreadsWithoutInheritance();

        a.first().start();
        a.second().start();
    }

    public Thread first() {
        return new Thread(() -> {
            for (int i = 1; i < 7; i++) {
                result += Math.pow(i, i);
                System.out.println(Thread.currentThread().getName() + " " + result); // Вывожу название потока и результат
                try {
                    Thread.sleep(1000); // Усыпляю поток (перевожу в режим ожидания) на секунду
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Thread second() {
        return new Thread(() -> {
            for (int i = 1; i < 7; i++) {
                result += Math.pow(i, 3);
                System.out.println(Thread.currentThread().getName() + " " + result); // Вывожу название потока и результат
                try {
                    Thread.sleep(1000); // Усыпляю поток (перевожу в режим ожидания) на секунду
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
