import java.util.ArrayList;
import java.util.Comparator;

public class Male extends Person implements ActiveInterface {
    private String features;
    private Work work;
    private ArrayList<String> favouriteClubs; // String, Work, ....

    public Male(String name, int age, char gender, String email, String features, Work work, ArrayList<String> favouriteClubs) {
        super(name, age, gender, email); // super(...) -> ключевое слово, позволяющее вызвать конструктор родителя
        // вызываем конструктор Person и передаем туда наши значения
        this.features = features; // this -> ключевое слово, показывающее, что мы обращаемся к полю класса
        this.work = work;
        this.favouriteClubs = favouriteClubs;
    }


    @Override
    void voice() {
        System.out.println("I am male");
    }

    @Override // Переопределение метода (toString из нашего Object)
    public String toString() {
        return "Male{" +
                "features='" + features + '\'' +
                ", work=" + work +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", gender=" + getGender() +
                ", email='" + getEmail() + '\'' +
                '}';
    }

    public String getFeatures() {
        return features;
    }

    public Work getWork() {
        return work;
    }

    public ArrayList<String> getFavouriteClubs() {
        return favouriteClubs;
    }


    public ArrayList<String> sortNatural() {
        // {"c", "b", "a"}
        favouriteClubs.sort(Comparator.naturalOrder());
        // {"a", "b", "c"}
        return favouriteClubs;
    }

    public int amountOfWords() {
        // Я хочу, чтобы мы вернули количество строк из ArrayList, длина которых больше 4х
        // get(i) -> i-й элемент Листа
        int cnt = 0;
        for (int i = 0; i < favouriteClubs.size(); i++) {
            if (favouriteClubs.get(i).length() > 4) {
                cnt++;
            }
        }
        return cnt;
    }

    @Override
    public void isActive() {
        System.out.println("Male");
    }
}
