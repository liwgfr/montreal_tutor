import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        // Считывание из файла (текстового)
        // Способов считать много.
        File file = new File("com/acm/input.txt");
        int[] a = new int[]{1, 2, 3};
        for (int i = 0; i < a.length + 5; i++) { // unchecked
            System.out.println(a[i]);
        }

        try (Scanner in = new Scanner(file)) {
            // Hello World -> next() -> Hello. Hello world -> nextLine() -> Hello world
            // next(), nextLine(). next() - считывает слово!!. nextLine() -> всю строку
            while (in.hasNextLine()) { // hasNextLine() -> а есть ли еще строки?
                String s = in.nextLine();
                // in.nextLine() -> должен быть один (конкретно в нашем цикле). Потому что даже при проверки (то есть в условии) джава берет строку и переходит к следующей.
                if (s.isEmpty()) {
                    continue;
                }
                System.out.println(s);
            }

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File's name is not correct (path should be used or file is not available)");
            // Не всегда такой подход идеален. Все по ситуации.
        }
        System.out.println("BLABLA");

    }
}
