public abstract class Person {
    private String name; // null
    private int age; // 0
    private char gender; // ""
    private String email; // null

    // public Person() { } // по умолчанию есть, если внутри НЕ пишем логику - необязательно создавать

    public Person(String name, int age, char gender, String email) {
        this.name = name;
        this.age = age;
        // contains принимает строку!!!
        // если email НЕ содержит @ и gender НЕ M и гендер не F
        // | (в контексте условия) -> даже если одно из выражений true, все равно джава смотрит выражение ДО КОНЦА
        // || (Lazy) -> если одно из выражений true, все остальное - пропускается
        // & (в контексте условия) -> даже если одно из выражений false, все равно джава смотрит выражение ДО КОНЦА
        // && (Lazy) -> если видим false, дальше уже не смотрим
        if (!email.contains("@") || (gender != 'M' && gender != 'F')) {
            throw new IllegalArgumentException("Wrong input");
        }
        this.gender = gender;
        this.email = email;

    }


    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    abstract void voice(); // abstract -> тело метода должно отсутствовать. Потому что раз метод абстрактный, он будет реализовываться в другом классе
    // void? -> ничего не возвращает

    void whoAmI() {
        System.out.println("I am human");
    }

    /**
     * Примитивные и ссылочные типы данных:
     * Примитивы:
     * byte -> целые числа. 1 байт. min=-2^7...max=2^7-1 (-128...127) 0 -> positive 1 байт - 8 бит. 2^(8-1) default 0
     * short -> целые числа. 2 байта. min=-2^15...max=2^15-1 0
     * int -> целые числа. 4 байта. min = -2^31...max=2^31-1 (2147...) 0 - positive number 0
     * long -> целые числа. 8 байт. min = -2^63...max=2^63-1. 0
     * boolean -> true/false. 1 byte. min=false...max=true false
     * char -> символ. 4 байта. 0...65535. Unicode -> самая большая кодировка. В ней как раз таки 65535 ячеек. 1085 -> '*'. 97 -> 'a', 150 -> 'A', "", " ", "\n" -> unicode. default - ""
     * float -> дробное число. 4 байта (6-7 знаков после точки)
     * double -> дробное число. 8 байт (12-14 знаков после точки)
     * Примитивы хранятся в стэке (Stack) -> как оперативная память у компа (места мало, но скорость высокая). // 8
     * Ссылочные типы данных хранятся в куче (Heap) -> как жесткий диск у компа (места много, но скорость меньше) // мы можем создавать свои ссылочные типы данных
     * 1) у примитивов нет методов
     * 2) примитивы полностью пишутся маленькими буквами
     * 3) Ссылочные типы данных инциализируеются с помощью new
     * Scanner in = new Scanner()
     */
}
