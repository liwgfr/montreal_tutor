import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Work workForSergey = new Work("Coding", 12.43);
        ArrayList<String> ar = new ArrayList<>();
        ar.add("PSG"); // ~10
        ar.add("FC Barcelona");
        ar.add("Chelsea");
        ar.add("FC Sheriff");
        Male m = new Male("Sergey", 19, 'M', "tdoktora@inbox.ru", "IT", workForSergey, ar);
        Female f = new Female("Amelia", 25, 'F', "qwe@zxc.asd", true, true);
        System.out.println(m.amountOfWords());
        System.out.println(m.sortNatural());

        // Java 1.0.
        // Все классы в Java унаследованы (мнимо) от класса Object.
        // Object obj = new Object();
        // obj.toString().
        // m.toInteger(); // ?
        System.out.println(f);
        System.out.println(m);
        // == ? equals
        int a = 5;
        int b = 5;
        // Через == сравниваем только примитивы (с оговоркой). Примитивы через == сравниваются ПО ЗНАЧЕНИЮ
        // Ссылочные типы данных сравниваем через реализованные equals (== будет сравнение ПО ССЫЛКАМ)
        String s = new String("Hello");
        String j = new String("Hello");
        System.out.println(a == b); // true
        System.out.println(s == j); // false.

        boolean isEqual = f.equals(m);

        System.out.println(isEqual);


    }
    // Абстракция - Подход к абстрагированию от каких-либо повторений и приведение к какому-то общему виду. Наша программа содержит "чертежи". Достигается с помощью абстрактных классов и интерфейсов.
    // Полиморфизм - (многоформенность) -> у тебя есть какой-то родительский класс (Parent) -> N дочерних (A, B, C...). В Parent есть метод void hi() -> однако у каждого класса дочернего поведение метода изменено. Полиморфизм достигается @Override (переопределением работы какого-либо метода)
    // Инкапсуляция - В джава нет функций - все является объектом. Подход для сокрытия реализации.
    // 1) Это означает, что инкапсуляция позволяет ограничивать какие-либо объекты (доступ, видимость...)
    // a) Во-первых мы делаем наш код безопаснее.
    // b) В разграничении ответственности
    // private, getter + setter
    // private -> доступ к методу/классу/переменной можно получить только внутри конкретного класса
    // Модификаторы доступа:
    // public -> Доступ из любого места
    // protected -> доступ из той же папки ИЛИ от унаследованного класса
    // default (просто пустота) -> доступ из той же папки

}
