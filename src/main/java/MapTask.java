
import java.util.Scanner;
import java.util.TreeMap;

public class MapTask {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        TreeMap<String, Integer> map = new TreeMap<>(); // Сбалансированное АВЛ красно-черное дерево. Сложность всего - O(logN)

        // HashMap vs LinkedHashMap vs TreeMap
        // 1) HashMap -> основан на hashcode + equals(доп проверка на тот случай если вдруг hash совпали - коллизия). Данные НЕ упорядочены
        // 2) LinkedHashMap -> HashMap: "qwe", "hi", "hello", "qwe", "hi" -> out: "hello", "qwe", "hi. Данные как ввели так и вывели
        // 3) TreeMap -> Основан на hashcode + equals(). "4", "3", "2" -> out: "2", "3", "4". TreeMap у нас по умолчанию сортирует по возрастанию. То есть в TreeMap мы можем настраивать нашу сортировку.
        //   >= 2 <=
        //    3  4
        //  3
        // 2 4

        // get() -> ArrayList -> O(1)
        // get() -> LinkedList -> O(n)


        // add
        // ? -> put()
        // in.nextLine();
        for (int i = 0; i < n; i++) {
            /*
            String line = in.nextLine();
            String[] array = line.split(" ");
            map.put(array[0], Integer.parseInt(array[1]));

             */
            String city = in.next();
            int value = in.nextInt();
            map.put(city, value);
        }
        for (int i = 0; i < map.size(); i++) {
            System.out.print(map.keySet().toArray()[i] + " " + map.get(map.keySet().toArray()[i]) + " ");
        }
        System.out.println();
        for (String s : map.keySet()) { // for (Тип данных название переменной : *коллекция по которой бежим*)
            System.out.print(s + " " + map.get(s) + " "); // map.get(*ключ*) мы достаем значение по ключу
        }
    }
}
/* todo
    Вводится число n, а затем n строк и n чисел
    Пример:
    5
    Москва 2
    Питер 10
    Лондон -2
    Флоренция 4
    Андорра 100
    Чтобы данные сохранились в виде ключ-значение и страны были отсортированы по возрастанию (лексикографически)
    Вывести через пробел
 */

class Chelovek {
    private String name;
    private int age;
    private String email;

    // hashCode -> основан на всех полях

}
