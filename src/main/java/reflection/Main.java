package reflection;

import jdk.jfr.Description;
import lombok.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {

    // Reflection API -> рефлексия в контексте Java это механизм само исследования кода
    // С помощью рефлексии мы можем получать доступ к сторонним (или нашему текущему) классу, полям, методам
    // Рефлексия с точки зрения эффективности медленная, но бывает полезной для получения каких-то значений полей в режиме Runtime
    // +
    // Мы можем исследовать поведение или значение каких-то методов/полей
    // Хорошо подходит для аналитических/тестировочных целей
    // -
    // Медленно работает
    // Нарушает принцип инкапсуляции (да и безопасность в целом)
    // Мы можем залезть и сломать то, что не надо нам ломать (private final -> сломалось)
    @SuppressWarnings(value = "all")
    public static void main(String[] args) {
        Person q = new Person("qweqwe", 32, "zxcxzc@asd.com");
        q.setName("jjj");
        System.out.println(q);

        //int[] arr = new int[-1];
        // Как начать нашу "рефлексию"?
        // Получаем сначала доступ к классу (чтобы оттуда взаимодействовать с полями, методами, конструкторами, интерфейсы, аннотации...)
        // 1
        try {
            Class<?> cl = Class.forName("reflection.Person"); // ? -> wildcard (generic)
            // System.out.println(cl);
            // generic класс Class
            // Class.forName() принимает строку и пытается найти класс по такому названию
            // Название класса - НЕ ГАРАНТИЯ, что он именно Person.class
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // 2
        // Animal animal = new Dog()// dog extends animal
        Person p = new Person(19, "binocla@vk.com"); // child extends Person child -> Person
        // класс, который мы получим от getClass ГАРАНТИРОВАНО является Person или ниже (дочерним, например)
        Class<? extends Person> clazz = p.getClass(); // getClass у Person
        // System.out.println(Arrays.toString(clazz.getDeclaredFields()));

        // 3
        Class<Person> clz = Person.class; // как по-мне самый удобный способ

        // clz.getName() // название класса (полное название, то есть если он лежит в папке, будет reflection.Person
        // clz.getSimpleName() // Название класса (только само название, то есть Person)
        // Class
        // Method
        // Field
        // Constructor...
        try {
            Field f = clz.getField("age"); // getField работает только с НЕ private полями
            System.out.println(f.get(p)); // field.get(экземпляр нашего класса у которого нужно что-то заполучить) (замена Getter)
            System.out.println(p);
            f.set(p, 21); // у экземпляра класса (p) поставить новое значение поля (замена Setter)
            System.out.println(f.get(p));
            System.out.println(p);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        // Получаем доступ к private
        try {
            Field f = clz.getDeclaredField("name"); // getDeclaredField работает только со всеми полями
            f.setAccessible(true); // этот метод позволяет нам работать с private
            System.out.println(f.get(p)); // field.get(экземпляр нашего класса у которого нужно что-то заполучить) (замена Getter)
            System.out.println(p);
            f.set(p, "Sergey"); // у экземпляра класса (p) поставить новое значение поля (замена Setter)
            System.out.println(f.get(p));
            System.out.println(p);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        for (Field f : clz.getDeclaredFields()) { // getDeclaredFields (getFields для non-private) возвращает Field[]
            System.out.println(f.getType() + " " + f.getName()); // getType() -> тип данных конкретного поля
        }

        try {
            Method m = clz.getMethod("voice");
            Method m1 = clz.getMethod("calc", int.class, int.class);
            m.invoke(p); // invoke -> запустить, он принимает экземпляр класса
            System.out.println(m1.invoke(p, 2, 5)); // мы только вызвали метод с параметрами
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        for (Method m : clz.getDeclaredMethods()) {
            System.out.println(Arrays.toString(m.getParameterTypes()) + " " + m.getReturnType() + " " + m.getName());
        }

        /* todo
            Конструкторы, Аннотации
         */

        try {
            Person newPerson = clz.getConstructor(String.class, int.class, String.class).newInstance("Jack", 123, "qweqwe@qweqwe.com"); // создаем экземпляр класса
            System.out.println(newPerson);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        try {
            Annotation[] annotations = clz.getMethod("voice").getAnnotations();

            System.out.println(Arrays.toString(annotations));
            for (Annotation a : annotations) {
                System.out.println(a);
                System.out.println(a.annotationType().getField("name").get(p));
                System.out.println(a.annotationType().getField("a").get(p));
                System.out.println(a.annotationType().getMethod("currentMethodName").invoke(a));
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        // Person newPesonStandard = new Person - аналог newInstance("Jack", 123, "qweqwe@qweqwe.com");
    }
}


@Setter
@Getter
@ToString
@AllArgsConstructor
// @NoArgsConstructor
class Person {
    private String name;
    public int age;
    String email;

    public Person(int age, String email) {
        this.age = age;
        this.email = email;
    }

    @Interesting(currentMethodName = "voice")
    public void voice() {
        System.out.println("Hello");
    }

    @Interesting(currentMethodName = "calc1")
    public int calc(int x, int y) {
        return x + y;
    }

    @Interesting(currentMethodName = "calc2")
    public int calc(int x, int y, int z) {
        return x + y + z;
    }

}
