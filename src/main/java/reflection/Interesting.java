package reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// Аннотация - тот же интерфейс, но с @ перед названием
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME) // RUNTIME позволяет взаимодействовать с аннотацией в рефлексии
public @interface Interesting {
    // Аннотации нужны для чего?
    // 1) Для разработчиков (@Override)
    // 2) Для JVM (@SuppressWarnings)
    // 3) Для кодогенерации/рефлексии...
    int a = 10;
    String name = "Working";
    String currentMethodName() default "Hello";
    // У аннотаций есть настройки. @Target, @Retention
    // @Target -> настройка нашей аннотации (настраивает область применения) -> К полю, к методу, ко всему
    // @Retention -> настройка компиляции аннотации (аннотация сохраняется в байт код, аннотация не сохраняется в байт код (runtime), аннотация сохраняется, но доступа к ней нет)
}
