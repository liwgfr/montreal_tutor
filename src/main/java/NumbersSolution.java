import java.util.Arrays;
import java.util.Scanner;

public class NumbersSolution {

    static final String[] numbersInWords = {"минус", "ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь",
            "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать",
            "семнадцать", "восемнадцать", "девятнадцать", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят",
            "семьдесят", "восемьдесят", "девяносто", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот",
            "восемьсот", "девятьсот", "тысяча", "милион", "милиард", "триллион"};
    static final String[] numbersInDigits = {"-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13",
            "14", "15", "16", "17", "18", "19", "2", "3", "4", "5", "6", "7", "8", "9", "1", "2", "3", "4", "5", "6", "7",
            "8", "9", "1", "1", "1", "1"};

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String line;
        while (true) {
            System.out.println("\nВведите сумму прописью для перевода в числовое представление строчных чисел (ввод пустой строки завершит программу): ");
            line = in.nextLine();
            if (line.isEmpty()) {
                break;
            }
            String[] input = line.split(" ");
            System.out.println(Arrays.toString(input));
            for (String x : input) {
                for (int j = 0; j < numbersInWords.length; j++) {
                    if (x.equals(numbersInWords[j])) {
                        System.out.print(numbersInDigits[j]);
                    } else {
                        continue;
                    }
                }
            }
        }
    }
}