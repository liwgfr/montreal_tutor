public class Female extends Person implements ActiveInterface {
    private boolean hasCat;
    private boolean hasPhone;

    public Female(String name, int age, char gender, String email, boolean hasCat, boolean hasPhone) {
        super(name, age, gender, email);
        this.hasCat = hasCat;
        this.hasPhone = hasPhone;
    }

    @Override
    void voice() {
        System.out.println("I am female");
    }

    @Override
    public String toString() {
        return "Female{" +
                "hasCat=" + hasCat +
                ", hasPhone=" + hasPhone +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", gender=" + getGender() +
                ", email='" + getEmail() + '\'' +
                '}';
    }

    @Override
    public void isActive() {
        System.out.println("Female");
    }
    // hashCode - хэш функция. Это сгенерированное уникальное (почти уникальное) число, которое зависит от полей нашего объекта + использует простые числа


    @Override
    public boolean equals(Object o) { // equals есть в Object. При переопределении - типы аргументов и их количество () не меняется!!!
        if (this == o) return true; // a.equals(a); сравнение с самим собой
        if (o == null || this.getClass() != o.getClass()) return false; // a.equals(null) | dom.equals(car)

        Female female = (Female) o; // если ничего выше не выполнилось, значит мы точно знаем, что наш Object o -> Female
        // female1.equals(female2);
        if (hasCat != female.hasCat) return false; // сравниваем по полям
        return hasPhone == female.hasPhone; // true, если равны
    }

    @Override
    public int hashCode() {
        int result = (hasCat ? 1 : 0);
        result = 31 * result + (hasPhone ? 1 : 0);
        return result;
    }

    /**
     * Тема для разбора на след занятии (done)
     *
     * @param
     * @return
     */


    public boolean isHasCat() {
        return hasCat;
    }

    public boolean isHasPhone() {
        return hasPhone;
    }
}
