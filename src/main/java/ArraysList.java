import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ArraysList {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>(); // Слева интерфейс - справа класс (!)

        list.add("Saratov");
        list.add("Kazan");
        list.add("Moscow");
        list.add("Montreal");
        list.sort(Comparator.naturalOrder()); // отсортировал по возрастанию
        list.sort(Comparator.reverseOrder()); // по убыванию
        // Сортировать можно только те классы, у которых реализованы equals & hashCode
        System.out.println(list);

        // Collection API Java 5.0
        // Data Structure -> определенное хранилище которое позволяет в удобном виде хранить что-либо
        // List -> Интерфейс (!!!). Этот интерфейс имеет некоторые заготовленные методы, которые реализуются по-своему в классах (ArrayList, LinkedList, CopyOnWriteArrayList...)
        // List extends Collection.
        // Все интерфейсы (List, Collection) и как следствие - классы (ArrayList, LinkedList...) являются Generic интерфейсами и классами.
        // Generic - обобщение. todo подробнее разобрать на следующем занятии
        /*
            ArrayList implements List
            List по структуре это динамический массив данных (однотипных). То есть он расширяется в зависимости от количества заполнения.
            Начальный размер List -> 16. увеличивается 1.73-74 (~2x) при заполнении ~70%
            Большая нотация (или complexity) -> O(1) -> const
            for (int i = 0; i < n; i++) {

            }
            for (int i = 0; i < n; i++) {

            }
            O(n)


            ArrayList ->
            0) add(index, element) -> add(3, 123) : O(n) average {1, 2, 3, 4, 5, 6}
            1) add() -> add(123) : O(1) -> вставляешь в конец
            2) get() -> O(1) -> (ar[2])
            3) remove() -> O(n)
            4) indexOf() -> indexOf возвращает позицию значения (первую) {1, 2, 3, 5, 3, 0}. -> O(n)
            5) contains() -> O(n) -> true/false
            ---------
            LinkedList ->
            Есть Head & Tail
            0) add(index, element) -> add(6, 3000) : O(n) average {1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7}
            1) add() -> add(123) : O(1) -> вставляешь в конец
            2) get() -> O(n) -> потому что чтобы дойти до N позиции "удава" - нужно всего его пройти
            3) remove(element) -> O(1)
            4) remove(index) -> O(n)
            5) contains() -> O(n)

            Difference:
             Arr: get() -> O(1) -> (ar[2]), remove(element) -> O(n)
             Link:  get() -> O(n) -> потому что чтобы дойти до N позиции "удава" - нужно всего его пройти,  remove(element) -> O(1)

            List
         */
    }
}
