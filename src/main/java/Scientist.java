import java.util.ArrayList;

public class Scientist extends Male {

    private final String fieldOfWork;
    private final float progress;

    public Scientist(String name, int age, char gender, String email, String features,
                     Work work, ArrayList<String> favouriteClubs, String fieldOfWork, float progress) {
        super(name, age, gender, email, features, work, favouriteClubs);
        this.fieldOfWork = fieldOfWork;
        if (progress < 0 || progress > 100) {
            throw new IllegalArgumentException("Wrong input");
        }
        this.progress = progress;
    }
}