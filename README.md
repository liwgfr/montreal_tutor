## Task

### City

Person (name, age, gender, email, void voice(), void whoAmI(System.out.println("I am human")))
gender - M / F, email "@"

Male(String features, Work work)
Female(boolean hasCat, boolean hasPhone)
Work(String name, double salary)

Scientist(String fieldOfWork, float progress (0-100)) -> Male ok Researcher(String fieldOfWork, boolean isDone) ->
Female ok

interface void isActive ("true" -> gender "Male", "false" -> gender "Female")

- Create & Complete entities
- implement Interface
- ...

Parent -> Person (Class, Enum, Record (Java 14 --preview+, Java 16 released), Abstract)
Children -> M, F Children x2 -> M -> Scientist, F -> Researcher

Work isActive

Record -> специальный новый тип класса, который позволяет упростить его написание (например, мнимо создает конструктор,
getter, setter, toString, equals, hashcode)
Abstract class vs Interface?

- А про какую версию джавы мы говорим?
- Java < 8
- Абстрактный класс - это класс, у которого обязательно должен быть хотя бы 1 абстрактный(нереализованный) метод. Есть
  конструктор. В интерфейсах конструктора нет + не может быть реализованных методов
- Java >= 8
- Различия между абстрактным классом и интерфейсом только в наличии у класса конструктора, в то время как у интерфейса
  его нет. Абстрактный класс - это класс, у которого обязательно должен быть хотя бы 1 абстрактный(нереализованный)
  метод
- default (определяет в интерфейсе поведение "по умолчанию")
- Проблема обратной совместимости. Java 8 -> Java 7. (besides new features)

*TODO: Задачи с acm.sgu.ru/lang. Регистрационные данные любые! Название класса - Solution. Ввод с клавиатуры*

*Задачи: 2007, 2011, 2005* todo check

### Plan

Input/Output Stream (Input / Output at all) [x]

Stream API (Lambda functions) [x]

Reflection API [x]

Concurrency API

Java SE Ended Java SE -> Java Standard Edition: синтаксис языка

Java EE (Spring, Quarkus, Hibernate, DataBases...): это все, что связано с Вебом

SQL / NoSQL

Spring + Quarkus ...